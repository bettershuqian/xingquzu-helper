<?php

namespace XHelper;

/**
 * 浏览器代理
 */
class UserAgentHelper
{
    /**
     * 默认的代理客户端信息
     * @var array
     */
    private static array $agents = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:88.0) Gecko/20100101 Firefox/88.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0 Safari/605.1.15',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:107.0) Gecko/20100101 Firefox/107.0',
    ];

    /**
     * 生成代理头
     * @param bool $autoAgent
     * @return string
     * @author pawn
     */
    public static function generate(bool $autoAgent = true): string
    {
        if ($autoAgent && isset($_SERVER['HTTP_USER_AGENT'])) {
            return $_SERVER['HTTP_USER_AGENT'];
        }

        return self::$agents[mt_rand(0, count(self::$agents) - 1)];
    }
}