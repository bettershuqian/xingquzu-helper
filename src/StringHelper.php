<?php

namespace XHelper;

class StringHelper
{
    /**
     * 隐私号码
     * @param string $phone
     * @return string
     */
    public static function privacyPhone(string $phone): string
    {
        //中国大陆手机
        if (strlen($phone) == 11) {
            return preg_replace('/^(\d{3})(?:\d{4})(\d{4})$/', "$1****$2", $phone);
        }

        //其他号码待定
        return $phone;
    }

    /**
     * 字符串长度
     * @param string $string 带计算的字符串
     * @param string $encoding 默认 utf-8
     * @return int
     */
    public static function count(string $string, string $encoding = 'utf-8'): int
    {
        return mb_strlen($string, $encoding);
    }

    /**
     * 长字符串提取
     * @param string $string
     * @param int $length 截取长度
     * @param string $suffix 超长字符占位符
     * @param int $start 截取起始位置
     * @param string $encoding 截取的编码方式
     * @return string
     */
    public static function abstract(
        string $string,
        int    $length = 150,
        string $suffix = '...',
        int    $start = 0,
        string $encoding = 'utf-8'
    ): string
    {
        if (self::count($string) <= $length) {
            return $string;
        }

        return mb_strcut($string, $start, $length - self::count($suffix), $encoding) . $suffix;
    }

    /**
     * 将单字节字符串分割成小块
     * @param string $string 被切割的字符串
     * @param int $length 每个块的长度
     * @param string $separator 分隔字符串
     * @return string
     */
    public static function chunk_split(string $string, int $length = 1, string $separator = ','): string
    {
        return implode($separator, str_split($string, $length));
    }

    /**
     * 首字母大写
     * @param string $string
     * @return string
     */
    public static function ucfirst(string $string): string
    {
        return ucfirst(self::lower($string));
    }

    /**
     * 字符串转小写
     * @param string $value
     * @return string
     */
    public static function lower(string $value): string
    {
        return mb_strtolower($value, 'UTF-8');
    }

    /**
     * 连字符转换为驼峰字符
     * ```php
     * a1_b2-c3 => a1B2C3
     * a1_b2_c3 => a1B2C3
     * a1-b2-c3 => a1B2C3
     * ```
     * @param string $string
     * @param string $separator
     * @return string
     */
    public static function camel(string $string, string $separator = '_-'): string
    {
        $array = preg_split('/[' . $separator . ']/', $string);
        return (string)array_reduce($array, function ($result, $val) {
            $result = $result ?: '';
            $result .= empty($result) ? self::lower($val) : self::ucfirst($val);
            return $result;
        });
    }
}