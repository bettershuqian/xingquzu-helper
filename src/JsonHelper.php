<?php
declare(strict_types=1);

namespace XHelper;

class JsonHelper
{
    /**
     * JSON编码
     * @param $value
     * @param int $options
     * @return false|string
     */
    public static function encode($value, int $options = 320)
    {
        return json_encode($value, $options);
    }

    /**
     * 解码JSON字符串
     * @param string $json
     * @param bool $asArray
     * @return mixed
     */
    public static function decode(string $json, bool $asArray = true)
    {
        return json_decode($json, $asArray);
    }
}