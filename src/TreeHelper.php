<?php

namespace XHelper;
/**
 * 递归处理类
 */
class TreeHelper
{
    /**
     * 子孙树
     * @param array $input 递归数据
     * @param string $pk 递归键名
     * @param int $pid 递归的起点
     * @param int $level 当前分类层级
     * @param string $key 递归父类键名
     * @return array
     */
    public static function sons(
        array  $input,
        string $pk = 'id',
        int    $pid = 0,
        int    $level = 1,
        string $key = 'pid'
    ): array
    {
        $output = [];
        foreach ($input as $val) {
            if ($val[$key] == $pid) {
                $val['level'] = $level;
                $output[] = $val;
                $output = array_merge($output, static::sons($input, $pk, $val[$pk], $level + 1, $key));
            }
        }

        return $output;
    }

    /**
     * 家谱树
     * @param string $pk 递归键名
     * @param array $input 递归的数据
     * @param int $id 递归的起点【谁的家谱树】
     * @param string $key 递归父类键名
     * @return array
     */
    public static function family(string $pk, array $input = [], int $id = 0, string $key = 'pid'): array
    {
        $family = [];
        foreach ($input as $val) {
            if ($val[$pk] == $id) {
                $family[] = $val;
                $family = array_merge(static::family($pk, $input, $val[$key], $key), $family);
            }
        }
        return $family;
    }

    /**
     * 子级节点
     * @param array $input 目标查询数据
     * @param int $id 子级节点的父级ID
     * @param string $key 父级键名
     * @return array
     */
    public static function children(array $input = [], int $id = 0, string $key = 'pid'): array
    {
        $sons = [];
        foreach ($input as $val) {
            if ($val[$key] == $id) {
                $sons[] = $val;
            }
        }
        return $sons;
    }

    /**
     * 无限级嵌套树
     * @param $data
     * @param int $level
     * @param int $id
     * @return array
     */
    public static function subtrees($data, int $level = 1, int $id = 0): array
    {
        $ret = [];
        foreach ($data as $key => $val) {
            if ($val['level'] == $level && $val['pid'] == $id) {
                $data[$key]['children'] = static::subtrees($data, $level + 1, $val['id']);
                $ret[] = $data[$key];
            }
        }
        return $ret;
    }
}
