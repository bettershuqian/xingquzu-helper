<?php

namespace XHelper;

class DateHelper
{
    /**
     * 时间戳格式化
     * @param int $timestamp
     * @param string $format
     * @return false|string
     */
    public static function format(int $timestamp, string $format = 'Y.m.d H:i:s')
    {
        if ($timestamp) {
            return date($format, $timestamp);
        }
        return '';
    }

    /**
     * js时间戳转php时间戳
     * @param int $timestamp
     * @return int
     */
    public static function js2Php(int $timestamp): int
    {
        if (strlen((string)$timestamp) === 13) {
            return (int)floor($timestamp / 1000);
        }
        return 0;
    }

    /**
     * 一对日期范围转时间戳范围
     * ```php
     * ['2023-01-14'] => [1673625600]
     * ['2023-01-14', '2023-01-14'] => [1673625600, 1673711999],
     * ['2023-01-14', '2023-01-15'] => [1673625600, 1673798399],
     * ```
     * @param array $date
     * @return array
     */
    public static function rangePair(array $date): array
    {
        //当仅选择了开始时间
        if (count($date) == 1) {
            $date[0] = strtotime($date[0]);
            return $date;
        }

        //选择了结束时间
        $date[0] = strtotime($date[0]);
        $date[1] = self::getEndOfDay($date[1]);
        return $date;
    }

    /**
     * 获取当天的结束时间戳
     * ```php
     *  2023-01-16 => 1673884799
     * ```
     * @param string $time
     * @return false|int
     */
    public static function getEndOfDay(string $time)
    {
        return strtotime("$time 23:59:59");
    }

    /**
     * 时间小尾巴
     * 例如 刚刚 10秒前 3分钟前 3小时前
     * @param int $timestamp
     * @return string
     */
    public static function tail(int $timestamp): string
    {
        $distance = time() - $timestamp;
        if ($distance < 5) {
            return '刚刚';
        }

        if ($distance < 60) {
            return "{$distance}秒前";
        }

        if ($distance < 3600) {
            return sprintf("%d", $distance / 60) . '分钟前';
        }

        if ($distance < 86400) {
            return sprintf("%d", $distance / 3600) . '小时前';
        }

        if ($distance < 86400 * 30) {
            return sprintf("%d", $distance / 86400) . '天前';
        }

        if ($distance < 86400 * 365) {
            return sprintf("%d", $distance / (86400 * 30)) . '月前';
        }

        if ($distance < 86400 * 365 * 5) {
            return sprintf("%d", $distance / (86400 * 365)) . '年前';
        }

        return static::format($timestamp);
    }
}